﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ClassThing1
{
    public class Person : IComparable <Person>
    {
        public int Id { get; private set; }
        public int Age { get; private set; }
        public string Name { get; private set; }
        public float Height { get; private set; }
        public Person(int id, int age, string name, float height)
        {
            Id = id;
            Age = age;
            Name = name;
            Height = height;
        }

        private static readonly IComparer<Person> idComparer = new PersonCompareById();
        private static readonly IComparer<Person> nameComparer = new PersonCompareByName();
        private static readonly IComparer<Person> ageComparer = new PersonCompareByAge();
        private static readonly IComparer<Person> hightComparer = new PersonComparedByHight();
        private static IComparer<Person> DefaultComparer;
        static public IComparer<Person> HightComparer
        {
            get
            {
                return hightComparer;
            }
        }
        static public IComparer<Person> AgeComparer
        {
            get
            {
                return ageComparer;
            }
        }
        static public IComparer<Person> NameComparer
        {
            get
            {
                return nameComparer;
            }
        }
        static public IComparer<Person> IDComparer
        {
            get
            {
                return idComparer;
            }
        }

        static Person()
        {
            idComparer = IDComparer;
            ageComparer = AgeComparer;
            hightComparer = HightComparer;
            nameComparer = NameComparer;
        }
        public static void ModifyDefaultComparer(string newDEfaultComparer)
        {
            switch (newDEfaultComparer)
            {
                case "name":
                    {
                        DefaultComparer = nameComparer;
                        break;
                    }
                case "age":
                    {
                        DefaultComparer = ageComparer;
                        break;
                    }
                case "Id":
                    {
                        DefaultComparer = idComparer;
                        break;
                    }
                case "hight":
                    {
                        DefaultComparer = hightComparer;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("enter one of persons properties");
                        break;
                    }
            }
        }
        public static IComparer<Person> GetDefaultComparer()
        {
            return DefaultComparer;
        }
        

        public override string ToString()
        {
            return $"name: {Name}, ID: {Id}, age: {Age}, hight: {Height}";
        }

        public int CompareTo(Person p)
        {
            return this.Id - p.Id;
        }
    }

}
