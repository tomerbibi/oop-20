using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ClassThing1
{
    class Program
    {
        public static void PrintPersonArray(Person[] people)
        {
            foreach (Person p in people)
            {
                Console.WriteLine(p);
            }
        }
        static void Main(string[] args)
        {
            Person[] people = new Person[5]
            {
                new Person(3, 5, "david",  1.80f),
                new Person(2, 6,  "sami", 1.90f),
                new Person(5, 28, "nirit", 1.55f),
                new Person(1, 56,  "shahar", 1.70f),
                new Person(6, 87, "moshe", 1.76f)
            };
            PrintPersonArray(people);
            Array.Sort(people);
            PrintPersonArray(people);
            Array.Sort(people, Person.NameComparer);
            PrintPersonArray(people);
            Array.Sort(people, Person.AgeComparer);
            PrintPersonArray(people);
            Array.Sort(people, Person.HightComparer);
            PrintPersonArray(people);
            Array.Sort(people, Person.IDComparer);
            PrintPersonArray(people);
            Person.ModifyDefaultComparer("name");
            Array.Sort(people, Person.GetDefaultComparer());
            PrintPersonArray(people);
        }
    }
}
